# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/


worker_processes auto;
error_log /var/log/nginx/error.log info;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    # perl_modules /opt/app-root/etc/perl;
    # perl_require Version.pm;
    # perl_set $perl_version Version::installed;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';


    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;

    # IMPORTANT NOTE: We break here with the CIS Benchmarks 2.4.3 and 2.4.4 as
    # THREDDS should be able to process long-running requests
    keepalive_timeout   300;
    send_timeout        300;
    types_hash_max_size 2048;
    server_names_hash_bucket_size 256;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /opt/app-root/etc/nginx.d/*.conf;

    server {
        listen       {{ cookiecutter.exposed_ports.split(',')[0].strip() }};
        listen       [::]:{{ cookiecutter.exposed_ports.split(',')[0].strip() }};
        server_name  {{ cookiecutter.server_name }};
        server_tokens off;
        client_max_body_size {{ cookiecutter.nginx_max_body_size }};
        client_body_timeout 10;
        client_header_timeout 10;
        large_client_header_buffers 2 1k;

        add_header X-Frame-Options SAMEORIGIN always;
        add_header X-Content-Type-Options "nosniff" always;

        {%- if cookiecutter.include_error_pages == "yes" %}
        proxy_intercept_errors on;
        {%- endif %}

        # root         /opt/app-root/src;

        # Load configuration files for the default server block.
        include /opt/app-root/etc/nginx.default.d/*.conf;

        # These settings should be configured in a file matching
        # nginx.default.d/*.conf
        # location / {
        # }

        # error_page 404 /404.html;
        #     location = /40x.html {
        # }

        # error_page 500 502 503 504 /50x.html;
        #     location = /50x.html {
        # }
    }

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
#        server_name  _;
#        client_max_body_size {{ cookiecutter.nginx_max_body_size }};
#        root         /opt/app-root/src;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers PROFILE=SYSTEM;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /opt/app-root/etc/nginx.default.d/*.conf;
#
#        location / {
#        }
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }

}
