# test script for NGINX CIS Benchmarks
#
# This script tests the relevant CIS Benchmarks from
# CIS NGINX Benchmark, v2.0.0 - 11-01-2022
#
# See https://downloads.cisecurity.org/#/ for reference
#
# Benchmark testing overview
# --------------------------
# The following benchmarks are considered in this test file.
# Benchmarks with a ✅ have been implemented, benchmarks with a ❌ are still
# missing, and the others are ignored as they are considered trivial in the
# ubi/rhel-based container-setup that we cover here (note that an ❌ does not
# necessarily mean that the benchmark is not implemented, it just means that
# it is not covered in this test suite).
#
# - 1.1.1 Ensure NGINX is installed (Automated)
# - 1.2.1 Ensure package manager repositories are properly configured (Manual)
# - 1.2.2 Ensure the latest software package is installed (Manual)
# - 2.2.1 Ensure that NGINX is run using a non-privileged, dedicated service account (Automated)
# - 2.2.2 Ensure the NGINX service account is locked (Automated)
# - 2.2.3 Ensure the NGINX service account has an invalid shell (Automated)
# - 2.3.1 Ensure NGINX directories and files are owned by root (Automated)
# - 2.3.2 Ensure access to NGINX directories and files is restricted (Automated)
# - 2.3.3 Ensure the NGINX process ID (PID) file is secured (Automated)
# - 2.4.1 Ensure NGINX only listens for network connections on authorized ports (Manual) ✅
# - 2.4.2 Ensure requests for unknown host names are rejected (Automated) ✅
# - 2.4.3 Ensure keepalive_timeout is 10 seconds or less, but not 0 (Automated) ✅
#         IMPORTANT NOTE: We break here with the CIS Benchmark as THREDDS
#         should be able to process long-running requests
# - 2.4.4 Ensure send_timeout is set to 10 seconds or less, but not 0 (Automated) ✅
#         IMPORTANT NOTE: We break here with the CIS Benchmark as THREDDS
#         should be able to process long-running requests
# - 2.5.1 Ensure server_tokens directive is set to off (Automated) ✅
# - 2.5.2 Ensure default error and index.html pages do not reference NGINX (Automated) (NOTE: these pages are not be used)
# - 2.5.4 Ensure the NGINX reverse proxy does not enable information disclosure ✅
# - 3.1 Ensure detailed logging is enabled (Manual) ❌ (needs further discussion with IT)
# - 3.2 Ensure access logging is enabled (Manual)
# - 3.3 Ensure error logging is enabled and set to the info logging level (Automated) ✅
# - 3.4 Ensure log files are rotated (Automated)
# - 3.7 Ensure proxies pass source IP information (Manual) ✅
# - 4.* is ignored because encryption is handled outside the application
# - 5.1.1 Ensure allow and deny filters limit access to specific IP addresses (Manual)
# - 5.1.2 Ensure only approved HTTP methods are allowed (Manual) ❌ (but implemented in the allowed_methods.conf)
# - 5.2.1 Ensure timeout values for reading the client header and body are set correctly (Automated) ✅
# - 5.2.2 Ensure the maximum request body size is set correctly (Automated) ✅
# - 5.2.3 Ensure the maximum buffer size for URIs is defined (Automated) ✅
# - 5.3.1 Ensure X-Frame-Options header is configured and enabled (Automated) ✅
# - 5.3.2 Ensure X-Content-Type-Options header is configured and enabled (Automated) ✅

setup() {
    load "test_helper/common-setup"
    _common_setup
}

maybe_skip() {
  if [ "$1" ]; then
    skip "Skipping due to presence of environment variable"
  fi
}


test_block() {
  if [[ "$1" =~ "$2" ]]; then
    return 1
  fi
}

@test "NGINX CIS 2.4.1 Ensure NGINX only listens for network connections on authorized ports" {
    maybe_skip "${SKIP_NGINX_2_4_1}"

    VALID_PORTS="({{ '|'.join(cookiecutter.exposed_ports.replace(' ', '').split(',')) }})"
    grep -inr --include='*.conf' "^\s*listen[^;]*;" /etc/nginx /opt/app-root/etc | sed 1d | \
    while read PORTS; do
      if test_block "$PORTS" "# skip: CIS 2.4.1"; then
        assert_regex "$PORTS" "$VALID_PORTS"
      fi
    done
}

@test "NGINX CIS 2.4.2 Ensure requests for unknown host names are rejected" {
    maybe_skip "${SKIP_NGINX_2_4_2}"

    ALL_SERVERS="$(grep -vr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/server {/,/}/p')"

    # check that the server_name is defined for each server block
    # and validate that it's not `_`
    while [[ "$ALL_SERVERS" =~ "server" ]]; do

      # extract content within brackets
      SERVER=${ALL_SERVERS#*'{'}
      SERVER=${SERVER%%'}'*}


      # strip this server block
      ALL_SERVERS=${ALL_SERVERS#*'}'}

      assert_regex "$SERVER" 'server_name'

      while read LINE; do
        if test_block "$LINE" "# skip: CIS 2.4.2"; then
          refute_regex "$LINE" "\s_(\s+|\;)"
        fi
      done < <(echo "$SERVER" | grep -i "server_name[^;]*;")
    done
}

@test "NGINX CIS 2.4.3 Ensure keepalive_timeout is 10 seconds or less, but not 0" {
    maybe_skip "${SKIP_NGINX_2_4_3}"

    run grep -inr --include='*.conf' "^\s*keepalive_timeout[^;]*;" /etc/nginx /opt/app-root/etc
    assert_output --partial keepalive_timeout

    while read LINE; do
      # we only allow values between 1 and 300
      # IMPORTANT NOTE: We break here with the CIS Benchmark as THREDDS should
      # be able to process long-running requests
      if test_block "$LINE" "# skip: CIS 2.4.3"; then
        assert_between "$LINE" "1" "300"
      fi
    done < <(grep -ir --include='*.conf' "^\s*keepalive_timeout[^;]*;" /etc/nginx /opt/app-root/etc)
}

@test "NGINX CIS 2.4.4 Ensure send_timeout is set to 10 seconds or less, but not 0" {
    maybe_skip "${SKIP_NGINX_2_4_4}"

    run grep -inr --include='*.conf' "^\s*send_timeout[^;]*;" /etc/nginx /opt/app-root/etc
    assert_output --partial send_timeout

    while read LINE; do
      # we only allow values between 1 and 300
      # IMPORTANT NOTE: We break here with the CIS Benchmark as THREDDS should
      # be able to process long-running requests
      if test_block "$LINE" "# skip: CIS 2.4.4"; then
        assert_between "$LINE" "1" "300"
      fi
    done < <(grep -ir --include='*.conf' "^\s*send_timeout[^;]*;" /etc/nginx /opt/app-root/etc)
}

@test "NGINX CIS 2.5.1 Ensure server_tokens directive is set to off" {
    maybe_skip "${SKIP_NGINX_2_5_1}"

    # NOTE: This is not the official audit from the Benchmark. The benchmark
    # recommends to check the response headers for the server header via::
    #
    #     curl -I 127.0.0.1 | grep -i server
    #
    # TODO: Add an integration test
    run grep -inr --include='*.conf' "^\s*server_tokens[^;]*;" /etc/nginx/nginx.conf
    assert_output --partial off
}


@test "NGINX CIS 2.5.4 Ensure the NGINX reverse proxy does not enable information disclosure" {
    maybe_skip "${SKIP_NGINX_2_5_4}"

    # get all location parameters and first strip comments
    ALL_LOCATIONS="$(grep -vnr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/location .* {/,/}/p')"

    while [[ "$ALL_LOCATIONS" =~ "location" ]]; do

      # extract content within brackets
      LOCATION=${ALL_LOCATIONS#*'{'}
      LOCATION=${LOCATION%%'}'*}


      # strip this location section
      ALL_LOCATIONS=${ALL_LOCATIONS#*'}'}

      if test_block "$LOCATION" "# skip: CIS 2.5.4"; then
        assert_regex "$LOCATION" "proxy_hide_header\s+X-Powered-By\s*;"
        assert_regex "$LOCATION" "proxy_hide_header\s+Server\s*;"
      fi
    done
}

@test "NGINX CIS 3.3 Ensure error logging is enabled and set to the info logging level" {
    maybe_skip "${SKIP_NGINX_3_3}"

    run grep -inr --include='*.conf' "^\s*error_log" /etc/nginx/nginx.conf
    assert_output --partial info
}


@test "NGINX CIS 3.7 Ensure proxies pass source IP information" {
    maybe_skip "${SKIP_NGINX_3_7}"

    # get all location parameters and first strip comments
    ALL_LOCATIONS="$(grep -vnr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/location .* {/,/}/p')"

    while [[ "$ALL_LOCATIONS" =~ "location" ]]; do

      # extract content within brackets
      LOCATION=${ALL_LOCATIONS#*'{'}
      LOCATION=${LOCATION%%'}'*}


      # strip this location section
      ALL_LOCATIONS=${ALL_LOCATIONS#*'}'}

      if [[ "$LOCATION" =~ "proxy_pass" ]]; then
        if test_block "$LOCATION" "# skip: CIS 3.7"; then
          assert_regex "$LOCATION" 'proxy_set_header\s+X-Real-IP\s+\$remote_addr\s*;'
          assert_regex "$LOCATION" 'proxy_set_header\s+X-Forwarded-For\s+\$proxy_add_x_forwarded_for\s*;'
        fi
      fi
    done
}

@test "NGINX CIS 5.2.1 Ensure timeout values for reading the client header and body are set correctly: client_body_timeout" {
    maybe_skip "${SKIP_NGINX_5_2_1}"

    # -------------------------------------------------------------------------
    # ------------- test for client_body_timeout ----------------------------
    # -------------------------------------------------------------------------

    # first a simple check if the client_body_timeout parameter is set anywhere
    run grep -inr --include='*.conf' "^\s*client_body_timeout[^;]*;" /etc/nginx /opt/app-root/etc
    assert_output --partial client_body_timeout


    # get all server blocks and first strip empty lines and comments
    ALL_SERVERS="$(grep -vr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/server {/,/}/p')"

    # check that the client_body_timeout is defined for each server block
    # and validate that it's between the limits
    while [[ "$ALL_SERVERS" =~ "server" ]]; do

      # extract content within brackets
      SERVER=${ALL_SERVERS#*'{'}
      SERVER=${SERVER%%'}'*}


      # strip this server block
      ALL_SERVERS=${ALL_SERVERS#*'}'}

      assert_regex "$SERVER" 'client_body_timeout'

      while read LINE; do
        # we only allow values between 1 and 60
        if test_block "$LINE" "# skip: CIS 5.2.1"; then
          assert_between "$LINE" "1" "60"
        fi
      done < <(echo "$SERVER" | grep -i "client_body_timeout[^;]*;")
    done
}

@test "NGINX CIS 5.2.1 Ensure timeout values for reading the client header and body are set correctly: client_header_timeout" {
    maybe_skip "${SKIP_NGINX_5_2_1}"

    # -------------------------------------------------------------------------
    # ------------- test for client_header_timeout ----------------------------
    # -------------------------------------------------------------------------

    # first a simple check if the client_header_timeout parameter is set anywhere
    run grep -inr --include='*.conf' "^\s*client_header_timeout[^;]*;" /etc/nginx /opt/app-root/etc
    assert_output --partial client_header_timeout


    # get all server blocks and first strip empty lines and comments
    ALL_SERVERS="$(grep -vr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/server {/,/}/p')"

    # check that the client_header_timeout is defined for each server block
    # and validate that it's between the limits
    while [[ "$ALL_SERVERS" =~ "server" ]]; do

      # extract content within brackets
      SERVER=${ALL_SERVERS#*'{'}
      SERVER=${SERVER%%'}'*}


      # strip this server block
      ALL_SERVERS=${ALL_SERVERS#*'}'}

      assert_regex "$SERVER" 'client_header_timeout'

      while read LINE; do
        # we only allow values between 1 and 60
        if test_block "$LOCATION" "# skip: CIS 5.2.1"; then
          assert_between "$LINE" "1" "60"
        fi
      done < <(echo "$SERVER" | grep -i "client_header_timeout[^;]*;")
    done
}

@test "NGINX CIS 5.2.2 Ensure the maximum request body size is set correctly" {
    maybe_skip "${SKIP_NGINX_5_2_2}"

    # first a simple check if the client_max_body_size parameter is set anywhere
    run grep -inr --include='*.conf' "^\s*client_max_body_size[^;]*;" /etc/nginx /opt/app-root/etc
    assert_output --partial client_max_body_size

    # get all server blocks and first strip empty lines and comments
    ALL_SERVERS="$(grep -vr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/server {/,/}/p')"

    # check that the client_max_body_size is defined for each server block
    # and validate that it's between the limits
    while [[ "$ALL_SERVERS" =~ "server" ]]; do

      # extract content within brackets
      SERVER=${ALL_SERVERS#*'{'}
      SERVER=${SERVER%%'}'*}


      # strip this server block
      ALL_SERVERS=${ALL_SERVERS#*'}'}

      if test_block "$SERVER" "# skip: CIS 5.2.2"; then
        assert_regex "$SERVER" 'client_max_body_size'
      fi
    done
}



@test "NGINX CIS 5.2.3 Ensure the maximum buffer size for URIs is defined" {
    maybe_skip "${SKIP_NGINX_5_2_3}"

    # we do only test for the presence of large_client_header_buffers. by
    # default this is implemented in the http block but one can overwrite this
    # setting in a specific server block if necessary

    run grep -inr --include='*.conf' "^\s*large_client_header_buffers[^;]*;" /etc/nginx/nginx.conf
    assert_output --partial "large_client_header_buffers"
}

@test "NGINX CIS 5.3.1 Ensure X-Frame-Options header is configured and enabled" {
    maybe_skip "${SKIP_NGINX_5_3_1}"

    # first a simple check if the X-Frame-Options header is added anywhere
    run grep -inr --include='*.conf' "X-Frame-Options" /etc/nginx /opt/app-root/etc
    assert_output --partial "X-Frame-Options"

    # get all server blocks and first strip empty lines and comments
    ALL_SERVERS="$(grep -vr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/server {/,/}/p')"

    # check that the X-Frame-Options is added in each server block
    # and validate that it's between the limits
    while [[ "$ALL_SERVERS" =~ "server" ]]; do

      # extract content within brackets
      SERVER=${ALL_SERVERS#*'{'}
      SERVER=${SERVER%%'}'*}


      # strip this server block
      ALL_SERVERS=${ALL_SERVERS#*'}'}

      if test_block "$SERVER" "# skip: CIS 5.3.1"; then
        assert_regex "$SERVER" 'add_header\s+X-Frame-Options'
      fi
    done
}

@test "NGINX CIS 5.3.2 Ensure X-Content-Type-Options header is configured and enabled" {
    maybe_skip "${SKIP_NGINX_5_3_1}"

    # first a simple check if the X-Content-Type-Options header is added anywhere
    run grep -inr --include='*.conf' "X-Content-Type-Options" /etc/nginx /opt/app-root/etc
    assert_output --partial "X-Content-Type-Options"

    # get all server blocks and first strip empty lines and comments
    ALL_SERVERS="$(grep -vr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/server {/,/}/p')"

    # check that the X-Content-Type-Options is added in each server block
    # and validate that it's between the limits
    while [[ "$ALL_SERVERS" =~ "server" ]]; do

      # extract content within brackets
      SERVER=${ALL_SERVERS#*'{'}
      SERVER=${SERVER%%'}'*}


      # strip this server block
      ALL_SERVERS=${ALL_SERVERS#*'}'}

      if test_block "$SERVER" "# skip: CIS 5.3.2"; then
        assert_regex "$SERVER" 'add_header\s+X-Content-Type-Options'
      fi
    done
}
