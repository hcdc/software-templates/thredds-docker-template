# Docker setup for {{ cookiecutter.project_slug }}

{% if cookiecutter.project_short_description %}
{{ cookiecutter.project_short_description }}
{% endif %}

This repository contains the files that are necessary to run and build the
images for the {{ cookiecutter.project_slug }} project.

## Installation

You need to have [docker](https://docs.docker.com/get-docker/) installed and
you need to clone this repository. We also recommend that you install
[`docker compose`](https://docs.docker.com/compose/install/).

Once you did this, create a folder named `{{ cookiecutter.project_slug }}` and
clone the `docker` repository:

```bash
# clone the repository
git clone https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/{{ cookiecutter.project_slug }}.git
# change into the cloned repository
cd {{ cookiecutter.project_slug }}
```
Now you need to build and deploy the images. We recommend that you do this via
`docker compose` (see below).

## Using openshift

We have a dedicated helm chart for this docker setup, accessible from
https://codebase.helmholtz.cloud/hcdc/kubernetes/charts/thredds

If you deploy this setup using this chart and made changes to the source code
of this repository, you need to run

```bash
oc start-build thredds
```

to trigger a new build.

## Using docker compose

The recommended way to build and deploy the files in this repository is to
use `docker compose`. You can install this manually for your operating system
(see the [docker docs](https://docs.docker.com/compose/install/)).

### Building the images

You first need to build the images locally via

```bash
docker compose build
```

### Running the containers

To run the containers, simply do a

```bash
docker compose up
```

or if you want to detach it, run

```bash
docker compose up -d
```

## Building the image manually

If you do not want to use `docker compose`, you can also build the image
manually via

```bash
docker build -t {{ cookiecutter.project_slug }}_thredds -f docker/thredds/Dockerfile .
```

## Running tests

Different containers have different test setups that test the images. Checkout
the [docker-compose.test.yml](docker-compose.test.yml) file to
see the different test services. To run the nginx-tests for instance, run

```bash
docker compose -f docker-compose.yml -f docker-compose.test.yml \
  run --build nginx-test
```

after you built the *nginx*-image (see above).

### NGINX CIS Benchmarks

This repository contains the tests for the containers, in particular we test
the following CIS Benchmarks (see https://downloads.cisecurity.org/#/),
*CIS NGINX Benchmark, v2.0.0 - 11-01-2022*

If you want to exclude a specific benchmark in your tests, set the
`SKIP_NGINX_?_?_?` variable. `?_?_?` should represent the number of the
benchmark. If you want to skip an individual block, add a `# skip: CIS ?.?.?`
comment behind the line that you want to skip, e.g.

```
server_name _;  # skip: CIS 2.4.2
```

#### Coverage of Benchmarks

Our test suite does not cover all benchmarks. Most of the benchmarks are
trivial in our ubi/rhel-based container setup. You can find a detailed list
in the corresponding test file:
[`docker/nginx/tests/test_cis.bats`](docker/nginx/tests/test_cis.bats)

Certain benchmarks need to be configured and checked manually:

- *3.1 Ensure detailed logging is enabled (Manual)*

  The discussion about what should be logged and how is ongoing.
- *2.5.1 Ensure server_tokens directive is set to off*

  The *Audit* of this benchmark recommends to test against a running nginx
  but our test suite here only checks against the configuration.
- *5.1.2 Ensure only approved HTTP methods are allowed (Manual)*

  allowed methods are implemented in the config file at
  [`docker/nginx/conf/nginx-default-cfg/allowed_methods.conf`](docker/nginx/conf/nginx-default-cfg/allowed_methods.conf).
  A test in the sense of continuous integration requires testing against a
  running nginx that is not possible in our current setup.

#### Example
To run the tests but skip benchmark 2.4.1,
(*2.4.1 Ensure NGINX only listens for network connections on authorized ports*),
run

```bash
docker compose \
  -f docker-compose.yml -f tests/docker-compose.test.yml \
  run -e SKIP_NGINX_2_4_1=1 --build nginx-test
```


## About this repository

The skeleton for this repository has been generated from the
[cookiecutter][cookiecutter] template at

https://codebase.helmholtz.cloud/hcdc/software-templates/nginx-docker-template

[cookiecutter]: https://cookiecutter.readthedocs.io/en/stable/

### Authors:
{%- for author_info in get_author_infos(cookiecutter.project_authors.split(','), cookiecutter.project_author_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}


### Technical note

This package has been generated from the template
{{ cookiecutter._template }}.

See the template repository for instructions on how to update the skeleton for
this package.


### Maintainers
{%- for author_info in get_author_infos(cookiecutter.project_maintainers.split(','), cookiecutter.project_maintainer_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}

## License information

Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

{% if cookiecutter.use_reuse == "yes" %}
Code files in this repository is licensed under the {{ cookiecutter.code_license }}.

Documentation files in this repository is licensed under {{ cookiecutter.documentation_license }}.

Supplementary and configuration files in this repository are licensed
under {{ cookiecutter.supplementary_files_license }}.

Please check the header of the individual files for more detailed
information.

{% else %}
All rights reserved.
{% endif %}
