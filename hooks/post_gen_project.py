import git
import os
import glob
import subprocess as spr
import glob
import shutil

{% if cookiecutter.include_maintenance_site == "no" %}
shutil.rmtree("docker/nginx/maintenance/")
os.remove("docker/nginx/conf/nginx-cfg/maintenance.conf")
{% endif %}

{% if cookiecutter.include_error_pages == "no" %}
shutil.rmtree("docker/nginx/error_pages/")
os.remove("docker/nginx/conf/nginx-default-cfg/404.conf")
{% endif %}

{% if cookiecutter.include_www_redirect == "no" %}
os.remove("docker/nginx/conf/nginx-cfg/redirect-www.conf")
{% endif %}

{% if cookiecutter.use_reuse == "yes" %}

spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "{{ cookiecutter.copyright_year }}",
        "--license",
        "{{ cookiecutter.documentation_license }}",
        "--copyright",
        "{{ cookiecutter.copyright_holder }}",
        "--recursive",
        "README.md",
        "docker/nginx/tests/test_helper/custom/README.md",
    ]
)

spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "{{ cookiecutter.copyright_year }}",
        "--license",
        "{{ cookiecutter.supplementary_files_license }}",
        "--copyright",
        "{{ cookiecutter.copyright_holder }}",
        "--recursive",
        ".gitignore",
        ".pre-commit-config.yaml",
        "docker-compose.yml",
        ".gitlab-ci.yml",
        "docker-compose.test.yml",
    ] + glob.glob("docker/thredds/conf/*")
)

spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "{{ cookiecutter.copyright_year }}",
        "--license",
        "{{ cookiecutter.supplementary_files_license }}",
        "--copyright",
        "{{ cookiecutter.copyright_holder }}",
        "--recursive",
        "--style",
        "python",
        "docker/nginx/conf/",
    ]
)

spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "{{ cookiecutter.copyright_year }}",
        "--license",
        "{{ cookiecutter.code_license }}",
        "--copyright",
        "{{ cookiecutter.copyright_holder }}",
        "--recursive",
        "docker/nginx/Dockerfile",
        "docker/nginx/tests/Dockerfile",
        "docker/nginx/tests/test_helper/common-setup.bash",
        "docker/nginx/tests/test_helper/custom/load.bash",
        "docker/nginx/tests/test_helper/custom/src/assert_between.bash",
        "docker/thredds/Dockerfile",
        "docker/thredds/entrypoint.sh",
    ]
)

spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "1998-2023",
        "--license",
        "BSD-3-Clause",
        "--copyright",
        "University Corporation for Atmospheric Research/Unidata",
        "--style",
        "html",
        "docker/thredds/wmsConfig.dtd",
    ]

)

spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "{{ cookiecutter.copyright_year }}",
        "--license",
        "{{ cookiecutter.code_license }}",
        "--copyright",
        "{{ cookiecutter.copyright_holder }}",
        "--recursive",
        "--style",
        "python",
    ] + glob.glob("docker/nginx/tests/*.bats")

)

spr.check_call(["reuse", "download", "--all"])

{% endif %}

print("Fixing files for project at {{cookiecutter.project_slug}}")
repo = git.Repo.init(".", mkdir=False)
{% if cookiecutter.git_remote_protocoll == "ssh" %}
repo.create_remote(
    "origin",
    "git@{{ cookiecutter.gitlab_host }}:{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git",
)
{% else %}
repo.create_remote(
    "origin",
    "https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git",
)
{% endif %}
repo.git.add(".")


spr.check_call(["pre-commit", "install"])

# make a silent run first to fix the files
spr.call(["pre-commit", "run"], stdout=spr.DEVNULL)

# add the files again after pre-commit hook has been run
repo.git.add(".")

spr.check_call(["pre-commit", "run"])

for dirpath, dirnames, filenames in os.walk("."):
    if "__pycache__" in dirnames:
        shutil.rmtree(os.path.join(dirpath, "__pycache__"))

{%- set counter = count(1) %}

print("""
===============================================================================
                          Contratulations!
===============================================================================
You just created a new docker-based project for deploying a django application.

{% if cookiecutter.use_reuse == "yes" -%}
+-------------------------------------------------------------+
| IMPORTANT NOTE:                                             |
|                                                             |
|    If you used cruft to create this package, please now run |
|    the following commands to stage the .cruft.json file and |
|    assign it the correct license:                           |
|                                                             |
+-------------------------------------------------------------+

{{ counter | next }}. Set the correct license information:

   $ reuse --root {{ cookiecutter.project_slug }} annotate --year {{ cookiecutter.copyright_year }} --license {{ cookiecutter.supplementary_files_license }} --copyright "{{ cookiecutter.copyright_holder }}" {{ cookiecutter.project_slug }}/.cruft.json

{{ counter | next }}. Stage the file:

   $ git -C {{ cookiecutter.project_slug }} add .cruft.json .cruft.json.license

{%- else %}
+-------------------------------------------------------------+
| IMPORTANT NOTE:                                             |
|                                                             |
|    You can now push everything to gitlab!                   |
|                                                             |
+-------------------------------------------------------------+
{%- endif %}

{{ counter | next }}. Make your first commit via

   $ git -C {{ cookiecutter.project_slug }} commit -m "Initial commit"

{{ counter | next }}. Push to https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}
   with

   $ git -C {{ cookiecutter.project_slug }} push -u origin main

{%- if cookiecutter.include_build_secret == "yes" and cookiecutter.ci_token_variable != "CI_JOB_TOKEN" %}

{{ counter | next }}. Create the CI variable for the build secret at
   https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/-/settings/ci_cd#js-cicd-variables-settings
{%- endif %}

Finally we recommend that you register this package at
https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview
such that the maintainers of the template can support you with keeping your
skeleton up-to-date. You can do this by opening an issue with the following
URL:

https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview/-/issues/new?issue%5Bconfidential%5D=true&issue%5Btitle%5D=New%20template%20usage%20for%20{{ cookiecutter.project_slug }}%20from%20{{ cookiecutter._template.split('/')[-1] }}&issue%5Bdescription%5D=Dear%20HCDC%20support%2C%20I%27d%20like%20to%20register%20the%20following%20repository%0A%0Ahttps%3A//{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git%0A%0Athat%20is%20using%20the%20following%20template%0A%0A{{ cookiecutter._template.replace('https:', 'https%3A' )}}"
""")