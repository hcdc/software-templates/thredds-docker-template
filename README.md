# Cookiecutter template for a basic docker-based thredds setup

This repository provides the possibility to generate the basic structure of an
thredds deployment with docker.

## Usage

This template can be used to create a new docker-based deployment, and to
update a deployment that has been created with this template. The template
is made for the [cookiecutter][cookiecutter] library, but in order to be able
to update packages that have been created with this template, we will be using
[cruft][cruft].

[cookiecutter]: https://cookiecutter.readthedocs.io/en/stable/
[cruft]: https://cruft.github.io/cruft/

## Prerequisites

Make sure that you have the requirements in [requirements.txt](requirements.txt)
installed. You can do so by downloading it and running

``` bash
pip install -r requirements.txt
```

### Package creation

You can generate a new deployment with the following commands:

``` bash
cruft create --skip .git --skip .mypy_cache https://codebase.helmholtz.cloud/hcdc/software-templates/thredds-docker-template.git
```

This will prompt for some input, including a `project_slug` under which
you will then find the skeleton of a new deployment. See the
[parameters.rst](parameters.rst) file for information on the individual
items.

The newly created folder will be setup as a git repository and the new
files will be staged automatically. It will run all the formatters for
the package and ensures high quality of your code right from the
beginning.

You will then receive some final instructions on how to finish the
initial setup.

> **Note**
>
> You can also clone this repository via `git clone`, but we recommend to
> use the URL. Otherwise `cruft` will store the path to the local folder
> in the `.cruft.json` configuration file.


### Register template usage

When you created a package with this template, please register it at
https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview/.

This helps the maintainers of the template to get an overview and to support
you with the usage of the template.


### Package update

Code-quality tools and frameworks quickly evolved in recent years and it
is indeed quite challenging to stay up-to-date with the latest
developments. By using [cruft][cruft], we
ensure that you can focus on your code and outsource these code-quality
developments to this template.

To update a package that has been created with this template and
`cruft`,

1.  Change into the directory for the deployment setup that you created in the
    previous step with `cruft create`

    ``` bash
    cd <path-to-your-local-folder>
    ```

2.  Create a new branch here (let\'s say `update-skeleton`):

    ``` bash
    git branch update-skeleton
    git checkout update-skeleton
    ```

3.  use the `update` command of `cruft`, review the changes and apply
    them:

    ``` bash
    cruft update
    ```

4.  Make sure that everything is working by executing the test suite:

    ``` bash
    git add .
    pre-commit run
    docker-compose up
    ```

5.  If everything is working, commit the changes

    ``` bash
    git commit -m "Updated deployment skeleton"
    ```

6.  Now push the changes to the remote

    ``` bash
    git push origin update-skeleton
    ```

    and create a merge request.


## About the tools that your deployment source repository will use

Setups that are built with this template repository do automatically
use the following state-of-the-art tools for automated code formatting
and validation.

If you have any questions or troubles with these tools, please contact
the maintainers of this template rather sooner than later. You can
[open an issue in this repository][issues] or contact us at
<hcdc_support@hereon.de>.

[issues]: https://codebase.helmholtz.cloud/hcdc/software-templates/python-package-template/-/issues

### Formatters

Automated code formatters make your development faster and easier as you
do not have to worry about how to make your code readable. The formatter
cares about this. Setups that are built with this template have the
following formatters configured:

-   [pre-commit-hooks]: The following standard pre-commit hooks are used:

    - trailing-whitespace
    - mixed-line-ending
    - end-of-file-fixer
    - check-yaml

To run all formatters, we recommend that you stage the files (e.g. via
`git add`) and use `pre-commit run` (see below). This will format the
files and you can compare them to the staged versions.

### Validators

The package uses code validators to ensure that the coding style
follows good practices and to prevent errors in the code. These
validators are implemented in the configs for `pre-commit` and `tox`.

The generated package uses the following static code validators:

-   [reuse][reuse] for handling of licenses

[reuse]: https://reuse.readthedocs.io/

#### reuse

On every commit, `reuse` validates every file in your repository to test
whether there are correctly encoded license information. As such you
need to call [reuse annotate][annotate] on
every new file that you create (unless it is excluded from the version
control via the `.gitignore` file).

[annotate]: https://reuse.readthedocs.io/en/latest/usage.html#annotate


## Extending this template

This template is a very basic template for an arbitrary python package.
Depending on the libraries and frameworks that you use, you might want
to use this repository as a starting point to create your own template.
If you want to do so, you should [create a fork of this repository][fork]
and implement the changes or additions in your fork. By doing so, you
can always merge changes from this repository into your fork and stay
up-to-date with the code quality tools that we use here.

If you want to directly make improvements to this template, please
[do so in a merge request at the source code repository][merge-request]
or [create an issue][issue].

[fork]: https://codebase.helmholtz.cloud/hcdc/software-templates/python-package-template/-/forks/new
[merge-request]: https://codebase.helmholtz.cloud/hcdc/software-templates/python-package-template/-/merge_requests/new
[issue]: https://codebase.helmholtz.cloud/hcdc/software-templates/python-package-template/-/issues